service {
  id = "app"
  name = "app"

  check {
    id = "app"
    name = "App Status"
    notes = "App checks every 10 seconds."
    ttl = "30s"
  }
}
