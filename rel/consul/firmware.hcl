service {
  id = "firmware"
  name = "firmware"

  check {
    id = "firmware"
    name = "Firmware Status"
    notes = "Firmware checks every 10 seconds."
    ttl = "30s"
  }
}