defmodule Railyard.DslTest do
  use ExUnit.Case

  # doctest Railyard.Base.Storage

  defmodule TestStack do
    use Railyard.Dsl

    resource :test, Railyard.Test do
      prop("value")
    end
  end

  test "evaluates a property" do
    assert Railyard.Util.BlockMap.eval_property(TestStack.test().block, :prop, []) ==
             {:prop, "value"}
  end

  # test "makes blocks" do
  #   b =
  #     BlockMap.create_block do
  #       prop3("qwerty")

  #       prop2(5678)

  #       prop do
  #         cond do
  #           true -> parameters.test
  #         end
  #       end
  #     end

  #   assert BlockMap.eval_block(b, test: 1234) == %{
  #            prop3: "qwerty",
  #            prop2: 5678,
  #            prop: 1234
  #          }
  # end
end
