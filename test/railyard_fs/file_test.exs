defmodule Railyard.Fs.FileTest do
  use ExUnit.Case
  use Railyard.ResourceSpec

  defmodule FileTester do
    use Railyard.Dsl

    resource :test_file, Railyard.Fs.File do
      path("./test.txt")
      contents(params.file_content)
    end
  end

  defmodule DirTester do
    use Railyard.Dsl

    resource :test_dir, Railyard.Fs.Directory do
      path("./test_dir")
    end
  end

  test "manages files" do
    FileTester.ship(file_content: "test")
    assert File.read!("./test.txt") == "test"

    FileTester.ship(file_content: "test2")
    assert File.read!("./test.txt") == "test2"

    FileTester.ship(:delete)
    assert File.exists?("./test.txt") == false
  end

  test "manages directories" do
    DirTester.ship([])
    assert File.dir?("./test_dir")

    DirTester.ship(:delete)
    assert File.exists?("./test_dir") == false
  end
end
