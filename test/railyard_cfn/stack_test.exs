defmodule Railyard.Cfn.StackTest do
  use ExUnit.Case

  test "makes stacks" do
    stack =
      Railyard.Stack.define do
        home_dir = 
          Railyard.Fs.directory(path: "~")

        dot_ssh =
          resource(Railyard.Fs.Directory,
            path: ref(home_dir).path |> File.join(".ssh")
          )

        authorized_keys =
          resource(Railyard.Fs.File,
            path: ref(dot_ssh).path |> Path.join("authorized_keys"),
            content: "asdf"
          )
        
        zshrc =
          resource(Railyard.Fs.File, :zshrc,
            path: ref(home_dir).path |> Path.join(".zshrc"),
            content: "echo hello world"
          )

        bashrc =
          resource(Railyard.Fs.File, :bashrc,
            path: ref(home_dir).path |> Path.join("~/.bashrc"),
            content: "echo hello world"
          )
        
        %{
          zshrc: ref(zshrc),
          bashrc: ref(bashrc)
        }
      end
    
    stack_pid =
      Railyard.Stack.start_link(stack)

    Railyard.Stack.ship(stack_pid, :zshrc, content: "echo hello zshrc")
    Railyard.Stack.ship(stack_pid, :bashrc, content: "echo hello bashrc")
  end
end
