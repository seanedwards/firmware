defmodule Railyard.MixProject do
  use Mix.Project

  def project do
    [
      app: :railyard,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package(),
      dialyzer: [
        flags: [:no_unused],
        plt_add_apps: [:mix, :distillery]
      ],

      # Docs
      name: "Railyard",
      source_url: "https://gitlab.com/seanedwards/firmware",
      homepage_url: "https://hexdocs.pm/railyard",
      docs: [
        main: "readme",
        extras: [
          "README.md",
          "CHANGELOG.md"
        ],
        groups_for_modules: [
          Providers: [
            # Please keep these in alphabetical order.
            Railyard.Cmd.Shell,
            Railyard.Distillery,
            Railyard.Fs.Directory,
            Railyard.Fs.File
          ],
          Integrations: [
            Mix.Tasks.Railyard,
            Railyard.Distillery.Plugin
          ],
          Utilities: [
            Railyard.Util.BlockMap
          ]
        ]
        # filter_prefix: "Railyard"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Railyard.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:dialyxir, "~> 1.0.0-rc.6", only: :dev, runtime: false},
      {:mojito, "~> 0.6"},
      {:jason, "~> 1.1"},
      {:sweet_xml, "~> 0.6"},
      {:configparser_ex, "~> 2.0"},
      {:ex_aws, "~> 2.1"},
      {:floki, "~> 0.25"},
      {:distillery, "~> 2.0", runtime: false},
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false},
      {:libcluster, "~> 3.0"},
      {:delta_crdt, "~> 0.5"},
      {:botocore, github: "boto/botocore", compile: false, app: false, runtime: false}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end

  defp description do
    "Configuration and infrastructure as code in Elixir"
  end

  defp package do
    %{
      # These are the default files included in the package
      files: ~w(lib priv .formatter.exs mix.exs *.md),
      licenses: ["AGPL"],
      links: %{"GitLab" => "https://github.com/seanedwards/firmware"}
    }
  end
end
