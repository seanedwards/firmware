# Ideas


    use Railyard
    @app_version Mix.Project.config[:version]

    # This attaches to the project's distillery configuration.
    # Mix tasks can handle bundling releases and upgrades, an duploading them
    # to S3. On the server, it describes where the app should be installed.
    deployment =
      resource Railyard.Distillery do
        s3_location "s3://buckets/deployments"
        path "/opt/app"
        version @app_version
      end
    
    datadog_install =
      resource :install_agent, Railyard.Shell do
        script """
        # Do whatever is needed to install the datadog agent.
        """
      end
    
    datadog_sidecar =
      resource :run_agent, Railyard.Supervisor, depends_on: [datadog_install] do
        command "/usr/local/bin/datadog"
        args [
          "--foreground",
          "--key=#{parameters.datadog_key}"
        ]
      end

    instance =
      resource :my_named_instance, Railyard.Aws.Ec2.Instance do
        image_id "ami-12345"
        key_name "sean
        # ...etc

        # Some properties are used by the `Railyard.Aws.Ec2.Instance` provider
        # to bootstrap the instance. The instance comes online with instructions to
        # connect with the existing cluster, and continue provisioning as `:my_named_instance`

        bootstrap_deployment ^deployment

        # All values in this resource block are captured and escaped as macros, to be later
        # evaluated by `Code.eval_quoted` at the appropriate time. This particular hook
        # would run as soon as the application starts.

        hook_after_start Railyard.ship(^datadog_sidecar, parameters: parameters)
      end
    
    %Railyard.Client{}
    |> Railyard.ship(instance, my_deployment_version: "1.0.0")
    