defmodule Mix.Tasks.Railyard do
  @moduledoc false
  use Mix.Task

  @shortdoc "Runs the railyard provisioner"
  def run([module_arg]) do
    {:ok, _started_list} = Application.ensure_all_started(:railyard)

    module =
      module_arg
      |> String.to_existing_atom()

    module.ship([])
  end
end
