defmodule Mix.Tasks.Cfn do
  @moduledoc false
  use Mix.Task

  @rc_specs %{
    "ap-south-1" =>
      "https://d2senuesg1djtx.cloudfront.net/latest/gzip/CloudFormationResourceSpecification.json",
    "ap-northeast-3" =>
      "https://d2zq80gdmjim8k.cloudfront.net/latest/gzip/CloudFormationResourceSpecification.json",
    "us-east-1" =>
      "https://d1uauaxba7bl26.cloudfront.net/latest/gzip/CloudFormationResourceSpecification.json",
    "us-east-2" =>
      "https://dnwj8swjjbsbt.cloudfront.net/latest/gzip/CloudFormationResourceSpecification.json",
    "us-west-1" =>
      "https://d68hl49wbnanq.cloudfront.net/latest/gzip/CloudFormationResourceSpecification.json",
    "us-west-2" =>
      "https://d201a2mn26r7lk.cloudfront.net/latest/gzip/CloudFormationResourceSpecification.json"
  }

  @shortdoc "Fetches the latest Cloudformation metadata"
  def run([]) do
    Application.ensure_all_started(:mojito)

    @rc_specs
    |> Enum.map(fn {region, spec_url} ->
      Task.async(fn ->
        {:ok, %Mojito.Response{body: body, status_code: 200}} =
          Mojito.get(spec_url, [], pool: false)

        File.write!("priv/cfn/#{region}.json", body |> :zlib.gunzip())
      end)
    end)
    |> Enum.each(&Task.await/1)
  end
end
