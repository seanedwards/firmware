defmodule Railyard do
  @moduledoc """
  This is the main API for Railyard. Creating and managing resources will be done using this module.
  """
  alias Railyard.Resource
  alias Railyard.Resource.Spec

  @type resource_spec :: %Spec{} | [%Spec{}]
  @type ok_or_error :: {:ok, any()} | {:error, String.t()}
  @spec ship(resource_spec, Keyword.t()) :: ok_or_error
  def ship(resource_specs, bindings \\ [])

  def ship(%Spec{} = resource_spec, :delete) do
    resource_id = Spec.id(resource_spec)

    Railyard.Registry
    |> Registry.lookup(resource_id)
    |> Enum.map(fn {pid, nil} ->
      Resource.delete(pid)
    end)
  end

  def ship(%Spec{} = resource_spec, bindings) when is_list(bindings) do
    Resource.ship(resource_spec, bindings)
  end

  def ship(resource_specs, bindings) when is_list(resource_specs) do
    resource_specs
    # |> sort_specs_by_dependencies
    |> Enum.map(fn resource_spec -> ship(resource_spec, bindings) end)
  end

  defp sort_specs_by_dependencies(resource_specs) do
    sort_specs_by_dependencies(
      resource_specs,
      resource_specs,
      []
    )
  end

  defp sort_specs_by_dependencies(
         all_specs,
         [
           %Spec{
             attributes: [depends_on: dependency_names],
             name: spec_name
           } = spec
           | rest_of_specs
         ] = resource_specs,
         ordered_specs
       ) do
    dependencies =
      dependency_names
      |> Enum.map(fn
        %Spec{name: name} ->
          name

        {dependency_module, dependency_name} ->
          apply(dependency_module, dependency_name)

        dependency_name ->
          all_specs
          |> find_spec_by_name(dependency_name)
      end)

    dependencies =
      (dependencies ++ ordered_specs)
      |> Enum.uniq_by(fn
        %Spec{name: name} -> name
      end)

    case ordered_specs |> find_spec_by_name(spec_name) do
      nil ->
        [spec | ordered_specs]

      _ ->
        ordered_specs
    end
  end

  defp sort_specs_by_dependencies([], all_specs, ordered_specs), do: ordered_specs

  defp find_spec_by_name(resource_specs, search_for_name) do
    resource_specs
    |> Enum.find(fn
      %Spec{name: name} ->
        search_for_name == name
    end)
  end

  def via_tuple(name) do
    {:via, Registry, {Railyard.Registry, name}}
  end
end
