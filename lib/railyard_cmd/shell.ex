defmodule Railyard.Cmd.Shell do
  @moduledoc """
  Runs a shell command
  """
  use Railyard.Resource

  defimpl Railyard.Provider do
    alias Railyard.Resource

    defdelegate ship(rc, bindings, state), to: Railyard.Provider.Any
    defdelegate action(rc, action, bindings, state), to: Railyard.Provider.Any
    defdelegate cleanup(rc_spec, state), to: Railyard.Provider.Any
  end
end
