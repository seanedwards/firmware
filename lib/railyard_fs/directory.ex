defmodule Railyard.Fs.Directory do
  @moduledoc """
  Creates or updates properties of a directory on the filesystem
  """
  use Railyard.Resource

  defimpl Railyard.Provider do
    alias Railyard.Resource

    def ship(rc, bindings, state) do
      with {:path, path} <- Resource.eval_property(rc, :path, bindings),
           :ok <- File.mkdir_p(path) do
        {:ok, %{path: path}}
      else
        err -> err
      end
    end

    def cleanup(rc_spec, %{path: path}) do
      File.rmdir(path)
    end

    def cleanup(_rc_spec, nil) do
      :ok
    end
  end
end
