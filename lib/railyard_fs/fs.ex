defmodule Railyard.Fs do
  @moduledoc false

  alias Railyard.Resource
  require Logger


  def file(kwlist) do
    Railyard.Stack.resource(Railyard.Fs.File, kwlist)
  end







  def ship(%_{} = rc, bindings, path) do
    ship(path, Resource.eval_property(rc, :mode, bindings))
    ship(path, Resource.eval_property(rc, :uid, bindings))
    ship(path, Resource.eval_property(rc, :gid, bindings))
  end

  def ship(path, {any_property, any_value}) do
    with {:ok, stat} <- File.stat(path) do
      ship(path, stat, {any_property, any_value})
    end
  end

  def ship(path, nil), do: nil

  def ship(path, %File.Stat{mode: mode}, {:mode, new_mode}) when mode != new_mode do
    File.chmod(path, new_mode)
  end

  def ship(path, %File.Stat{uid: uid}, {:uid, new_uid}) when uid != new_uid do
    File.chown(path, new_uid)
  end

  def ship(path, %File.Stat{gid: gid}, {:gid, new_gid}) when gid != new_gid do
    File.chgrp(path, new_gid)
  end

  def ship(path, _any_stat, {any_property, any_value}) do
    Logger.debug("Skipping #{any_property} for #{path} because it is already set.")
    :ok
  end
end
