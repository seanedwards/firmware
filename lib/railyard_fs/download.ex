defmodule Railyard.Fs.Download do
  @moduledoc """
  Downloads a file over HTTP and writes it to the filesystem.
  """
  use Railyard.Resource

  defimpl Railyard.Provider do
    alias Railyard.Resource

    def ship(rc, bindings, state) do
      path = Resource.eval_property!(rc, :path, bindings)

      with {:ok, stat} <- File.stat(path) do
      else
        {:error, :eaccess} ->
          {:error, "Access denied"}

        {:error, _anything_else} ->
          action(rc, :download, bindings, %{state | path: path})
      end
    end

    def action(rc, :download, bindings, %{path: path} = state) do
      url = Resource.eval_property!(rc, :url, bindings)

      {:ok, response} = Mojito.get(url, [], pool: false)
      File.write(path, response.body)

      {:ok, state}
    end

    def cleanup(rc_spec, %{path: path}) do
      File.rm(path)
    end

    def cleanup(_rc_spec, nil) do
      :ok
    end
  end
end
