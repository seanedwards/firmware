defmodule Railyard.Fs.File do
  @moduledoc """
  Creates and manages a file on the filesystem.

  Fun fact: This was the first provider ever written!
  """
  use Railyard.Resource

  defstruct [
    :path
  ]

  def init(args) do
    {:ok, %__MODULE__{path: args.path}}
  end

  def handle_call({:ship, args}, _from, _state) do
    File.write(args.path, pargs.contents)
    state = %__MODULE__{path: args.path}
    {:reply, state, state}
  end

  def handle_cast(:cleanup, _from, state) do
    File.rm(state.path)
    :ok
  end
end
