defmodule Railyard.Application do
  @moduledoc """
  Starts a `Registry` to map resource spec names to `Railyard.Resource` process IDs,
  and a `DynamicSupervisor` to supervise those processes.
  """
  use Application
  require Logger

  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: Railyard.Registry},
      {DynamicSupervisor, strategy: :one_for_one, name: Railyard.ResourceSupervisor}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Railyard.Supervisor)
  end
end
