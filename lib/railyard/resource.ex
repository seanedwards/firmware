defmodule Railyard.Resource do

  defstruct [
    resource_module: nil,
    resource_state: nil,
    subscriptions: []
  ]
  use GenServer
  def start_link([__MODULE__,]) do
    GenServer.start_link(__MODULE__, {:init, id, args})
  end

  defmacro __using__(_opts \\ []) do
    quote do
      @doc false
      def handle_call(_msg, _from, state), do: {:reply, :err_not_handled, state}
      @doc false
      def handle_cast(_msg, state), do: {:noreply, state}
      @doc false
      def handle_info(_msg, state), do: {:noreply, state}
      @doc false
      def handle_continue(_msg, state), do: {:noreply, state}

      @doc false
      def terminate(_reason, _scene_state), do: :ok

      # child spec that really starts up scene, with this module as an option
      @doc false
      def child_spec({args, opts}) when is_list(opts) do
        %{
          id: make_ref(),
          start: {Railyard.Resource, :start_link, [__MODULE__, args, opts]},
          type: :worker,
          restart: :permanent,
          shutdown: 500
        }
      end

      defoverridable handle_call: 3,
                     handle_cast: 2,
                     handle_continue: 2,
                     handle_info: 2,
                     terminate: 2,
                     child_spec: 1

      def start_link(args) do
        GenServer.start_link(__MODULE__, {:init, id, args})
      end

      @after_compile Railyard.Resource
    end
  end

  defmacro __after_compile__(_env) do
    quote do
    end
  end

  def child_spec({resource_module, args, opts}) do
    if function_exported?(resource_module, :child_spec, 1) do
      resource_module.child_spec({args, opts})
    else
      %{
        id: make_ref(),
        start: {__MODULE__, :start_link, [resource_module, args, opts]},
        type: :worker,
        restart: :permanent,
        shutdown: 500
      }
    end
  end


  def init({:init, args}) do
    {:ok, args, continue: :make_props}
  end

  def handle_continue(:make_props, args) do
    properties =
      args
      |> Enum.map(fn
        {key, val_fn} when is_function(val_fn) ->
          {key, Task.async(fn ->
            val_fn.(key)
          end)}
        {key, val} -> {key, val}
      end)
      |> Task.async_all(:60_000)
    handle_cast({ship, properties}, nil)
  end

  def handle_call({:ship, _args}, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:subscribe, pid, state) do
    
  end

  def handle_cast({:ship, args}, state) do
    {:reply, _reply, state} = handle_call({:ship, args}, nil, state)
    {:noreply, state}
  end

  def handle_cast(:cleanup, _from, _state) do
    :ok
  end

  defp generate_api() do
    quote do
      @spec add(Railyard.Stack.t(), atom(), keyword()) :: Railyard.Stack.t()
      def add(stack \\ nil, name, kwlist) when is_atom(name) do
        Railyard.Stack.add_resource(stack, name, kwlist)
      end
    end
  end
end
