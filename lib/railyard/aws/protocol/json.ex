defmodule Railyard.Aws.Protocol.Json do
  def from_op(operation) do
    %Railyard.Aws.Operation{
      name: op_name_str,
      input: input_mod,
      http: %{
        "requestUri" => http_path
      },
      metadata: %{
        "protocol" => "json",
        "endpointPrefix" => endpoint_prefix,
        "targetPrefix" => target_prefix,
        "apiVersion" => version
      }
    } = operation.__struct__.op_spec()

    namespace = "#{target_prefix}_#{version |> String.replace("-", "")}"

    params =
      case input_mod do
        nil ->
          nil

        mod ->
          operation.input
      end

    ExAws.Operation.JSON.new(
      String.to_atom(endpoint_prefix),
      %{
        data: Jason.encode!(params),
        headers: [
          {"x-amz-target", "#{namespace}.#{op_name_str}"},
          {"content-type", "application/x-amz-json-1.0"}
        ]
      }
    )
  end
end
