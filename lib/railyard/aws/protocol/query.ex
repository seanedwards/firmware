defmodule Railyard.Aws.Protocol.Query do
  require Logger
  alias Railyard.Aws.Util, as: Util
  import SweetXml, only: [sigil_x: 2, transform_by: 2]

  def from_op(operation) do
    %Railyard.Aws.Operation{
      name: op_name_str,
      input: input_mod,
      http: %{
        "requestUri" => http_path
      },
      metadata: %{
        "protocol" => "query",
        "endpointPrefix" => endpoint_prefix,
        "apiVersion" => version
      }
    } = operation.__struct__.op_spec()

    params =
      case input_mod do
        nil ->
          %{}

        mod ->
          to_aws(mod.shape_spec(), operation.input)
      end
      |> Enum.flat_map(fn
        {_key, nil} ->
          []

        {key, val} when is_list(val) ->
          ExAws.Utils.format(val, type: :xml, prefix: "#{key}.member")

        {key, val} ->
          ExAws.Utils.format(val, type: :xml, prefix: key)
      end)

    %ExAws.Operation.Query{
      path: http_path,
      action: op_name_str,
      service: String.to_atom(endpoint_prefix),
      params: [
        {"Action", op_name_str},
        {"Version", version}
        | params
      ]
    }
  end

  def parse_response(operation, {:ok, %{body: xml}}) do
    %Railyard.Aws.Operation{
      name: op_name_str,
      output: output_mod,
      output_wrapper: wrapper
    } = operation.__struct__.op_spec()

    result =
      case wrapper do
        nil -> SweetXml.xpath(xml, ~x"./"e)
        w -> SweetXml.xpath(xml, ~x"./#{w}")
      end

    case output_mod do
      nil -> {:ok, nil}
      mod -> {:ok, parse(mod.shape_spec(), result)}
    end
  end

  def parse(%Railyard.Aws.Shape.Structure{module: module}, nil) do
    Kernel.struct(module, [])
  end

  def parse(%Railyard.Aws.Shape.Structure{module: module, members: members}, xml) do
    result =
      members
      |> Enum.map(fn {attr, {member_name, member_mod}} ->
        attr_xml = SweetXml.xpath(xml, ~x"./#{member_name}"e)

        {
          attr,
          parse(member_mod.shape_spec(), attr_xml)
        }
      end)

    Kernel.struct(module, result)
  end

  def parse(%Railyard.Aws.Shape.List{}, nil), do: []

  def parse(%Railyard.Aws.Shape.List{member: member_module}, xml) do
    xml
    |> SweetXml.xpath(~x"./member"el)
    |> Enum.map(&parse(member_module.shape_spec(), &1))
  end

  def parse(%Railyard.Aws.Shape.Map{}, nil), do: %{}

  def parse(%Railyard.Aws.Shape.Map{key_module: key_module, value_module: value_module}, xml) do
    xml
    |> SweetXml.xpath(~x".")
    |> Enum.map(fn {k, v} ->
      {
        parse(key_module.shape_spec(), k),
        parse(value_module.shape_spec(), v)
      }
    end)
    |> Enum.into(%{})
  end

  def parse(_, nil), do: nil

  def parse(%Railyard.Aws.Shape.Basic{type: t}, xml) when t in ["integer", "long"] do
    xml
    |> SweetXml.xpath(~x"./text()"s)
    |> String.to_integer()
  end

  def parse(%Railyard.Aws.Shape.Basic{type: "boolean"}, xml) do
    SweetXml.xpath(xml, ~x"./text()"s) == "true"
  end

  def parse(%Railyard.Aws.Shape.Basic{type: "timestamp"}, xml) do
    {:ok, timestamp, 0} =
      xml
      |> SweetXml.xpath(~x"./text()"s)
      |> DateTime.from_iso8601()

    timestamp
  end

  def parse(%Railyard.Aws.Shape.Basic{type: "string"}, xml) do
    xml |> SweetXml.xpath(~x"./text()"s)
  end

  defp to_aws(%Railyard.Aws.Shape.Structure{module: module, members: members}, %module{} = req) do
    members
    |> Enum.map(fn {property, {name, member_mod}} ->
      {
        name,
        case Map.get(req, property) do
          nil -> nil
          value -> to_aws(member_mod.shape_spec(), value)
        end
      }
    end)
    |> Enum.into(%{})
  end

  defp to_aws(%Railyard.Aws.Shape.List{member_name: m_name, member: m_mod}, list)
       when is_list(list) do
    %{
      m_name => list |> Enum.map(&to_aws(m_mod.shape_spec(), &1))
    }
  end

  defp to_aws(%Railyard.Aws.Shape.Basic{}, val) do
    val
  end
end
