defmodule Railyard.Aws.Debug.CompileTracer do
  require Logger

  def trace({:alias_reference, _meta, mod}, _env) do
    case inspect(mod) do
      "ExAws" <> _rest -> Logger.debug("Referencing #{mod}")
      _ -> nil
    end

    :ok
  end

  def trace({:struct_expansion, _meta, mod, _keys}, env) do
    env_mod = env.module

    case mod do
      m when m == env_mod ->
        nil

      m ->
        m_str = inspect(m)
        e_str = inspect(env_mod)

        if String.starts_with?(m_str, "ExAws.") && !String.ends_with?(e_str, ".Client") do
          Logger.debug("#{inspect(env_mod)} is expanding %#{inspect(m)}{}")
        end
    end

    :ok
  end

  def trace(_event, _env) do
    :ok
  end
end
