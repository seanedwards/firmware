defmodule Railyard.Aws.XmlParser do
  alias Railyard.Aws.Util, as: Util
  import SweetXml, only: [sigil_x: 2, transform_by: 2]

  def generate_parser(
        %{"metadata" => %{"protocol" => "query", "serviceId" => service_id}} = _service_json,
        {shape_name, %{"type" => "integer"} = _shape_spec},
        _required
      ) do
    shape_mod = Util.module_name(service_id, shape_name)

    quote do
      def parse(unquote(shape_mod), nil), do: nil

      def parse(unquote(shape_mod), elem) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        elem
        |> SweetXml.xpath(~x"./text()"s |> transform_by(&String.to_integer/1))
      end
    end
  end

  def generate_parser(
        %{"metadata" => %{"protocol" => "query", "serviceId" => service_id}} = _service_json,
        {shape_name, %{"type" => "boolean"} = _shape_spec},
        _required
      ) do
    shape_mod = Util.module_name(service_id, shape_name)

    quote do
      def parse(unquote(shape_mod), nil), do: nil

      def parse(unquote(shape_mod), elem) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        elem
        |> SweetXml.xpath(~x"./text()"s |> transform_by(&(&1 == "true")))
      end
    end
  end

  def generate_parser(
        %{"metadata" => %{"protocol" => "query", "serviceId" => service_id}} = _service_json,
        {shape_name, %{"type" => t} = _shape_spec},
        _required
      )
      when t in ["string", "blob"] do
    shape_mod = Util.module_name(service_id, shape_name)

    quote do
      def parse(unquote(shape_mod), nil), do: nil

      def parse(unquote(shape_mod), elem) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        elem
        |> SweetXml.xpath(~x"./text()"s)
      end
    end
  end

  def generate_parser(
        %{"metadata" => %{"protocol" => "query", "serviceId" => service_id}} = _service_json,
        {shape_name, %{"type" => "timestamp"} = _shape_spec},
        _required
      ) do
    shape_mod = Util.module_name(service_id, shape_name)

    quote do
      def parse(unquote(shape_mod), nil), do: nil

      def parse(unquote(shape_mod), elem) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        elem
        |> SweetXml.xpath(~x"./text()"s)
      end
    end
  end

  def generate_parser(
        %{"metadata" => %{"serviceId" => service_id, "protocol" => "query"}} = _service_json,
        {shape_name, %{"type" => "map", "key" => key, "value" => val} = _shape_spec},
        _required
      ) do
    key_module = Util.module_name(service_id, key)
    val_module = Util.module_name(service_id, val)
    shape_mod = Util.module_name(service_id, shape_name)

    quote do
      def parse(unquote(shape_mod), nil), do: %{}

      def parse(unquote(shape_mod), elem) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        elem
        |> SweetXml.xpath(~x".")
        |> Enum.map(fn {k, v} ->
          {parse(unquote(key_module), k), parse(unquote(val_module), v)}
        end)
        |> Enum.into(%{})
      end
    end
  end

  def generate_parser(
        %{"metadata" => %{"serviceId" => service_id, "protocol" => "query"}} = _service_json,
        {shape_name, %{"type" => "list", "member" => %{"shape" => member}} = _shape_spec},
        _required
      ) do
    member_module = Util.module_name(service_id, member)
    shape_mod = Util.module_name(service_id, shape_name)

    quote do
      def parse(unquote(shape_mod), nil), do: []

      def parse(unquote(shape_mod), elem) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        elem
        |> SweetXml.xpath(~x"./member"el)
        |> Enum.map(&parse(unquote(member_module), &1))
      end
    end
  end

  def generate_parser(
        %{"metadata" => %{"serviceId" => service_id, "protocol" => "query"}} = _service_json,
        {
          shape_name,
          %{
            "type" => "structure",
            "members" => members
          } = _shape_spec
        },
        _required
      ) do
    shape_mod = Util.module_name(service_id, shape_name)

    xpaths =
      members
      |> Enum.map(fn {attr_name, attr_spec} ->
        attr_module = Util.module_name(service_id, attr_spec)

        {
          attr_name
          |> Macro.underscore()
          |> String.to_atom(),
          attr_module,
          ~x"./#{attr_name}"e
        }
      end)

    quote do
      def parse(unquote(shape_mod), nil), do: %unquote(shape_mod){}

      def parse(unquote(shape_mod), elem) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        result =
          unquote(Macro.escape(xpaths))
          |> Enum.map(fn {attr_name, attr_module, attr_xpath} ->
            {attr_name, parse(attr_module, SweetXml.xpath(elem, attr_xpath))}
          end)

        Kernel.struct(unquote(shape_mod), result)
      end
    end
  end

  def generate_parser(
        %{"metadata" => %{"serviceId" => service_id, "protocol" => "query"}} = _service_json,
        %{"name" => op_name} = op_def,
        _required
      ) do
    op_mod = Util.module_name(service_id, op_name)

    parse_ok =
      case op_def["output"] do
        nil ->
          quote do
            def parse_response({:ok, _resp}, unquote(op_name), _config) do
              {:ok, nil}
            end
          end

        %{
          "shape" => _,
          "resultWrapper" => result_type
        } = output_name ->
          output_type = Util.module_name(service_id, output_name)

          quote do
            def parse_response({:ok, %{body: xml} = resp}, unquote(op_mod), config) do
              import SweetXml, only: [sigil_x: 2, transform_by: 2]

              result =
                xml
                |> SweetXml.xpath(~x"./#{unquote(result_type)}"e)

              {:ok, parse(unquote(output_type), result)}
            end
          end
      end

    quote do
      unquote(parse_ok)

      def parse_response({:error, {type, http_status_code, %{body: xml}}}, _, _) do
        import SweetXml, only: [sigil_x: 2, transform_by: 2]

        parsed_body =
          xml
          |> SweetXml.xpath(
            ~x"//ErrorResponse",
            request_id: ~x"./RequestId/text()"s,
            type: ~x"./Error/Type/text()"s,
            code: ~x"./Error/Code/text()"s,
            message: ~x"./Error/Message/text()"s,
            detail: ~x"./Error/Detail/text()"s
          )

        {:error, {type, http_status_code, parsed_body}}
      end
    end
  end
end
