defmodule Railyard.Resource.Spec do
  @moduledoc """
  Holds the template for a given resource. Resource Specs are data objects that, when combined
  with bindings, can be evaluated to produce a final value.
  """
  @type t :: %Railyard.ResourceSpec{
          module: atom(),
          name: atom(),
          type: atom(),
          attributes: [keyword()],
          block: term()
        }
  defstruct module: nil,
            name: nil,
            type: nil,
            attributes: [],
            block: nil

  defmacro __using__(_opts \\ []) do
    quote do
      require Railyard.ResourceSpec
      require Railyard.Util.BlockMap

      :ok
    end
  end

  @spec id(%Railyard.ResourceSpec{}) :: {atom(), atom()}
  def id(%Railyard.ResourceSpec{module: module, name: name}) do
    {module, name}
  end

  @spec eval_property(%Railyard.ResourceSpec{}, atom(), Keyword.t()) :: {atom(), any()}
  def eval_property(%Railyard.ResourceSpec{block: block}, property, bindings \\ []) do
    Railyard.Util.BlockMap.eval_property(block, property, bindings)
  end
end
