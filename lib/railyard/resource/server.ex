defmodule Railyard.Resource.Server do
  @moduledoc """
  You probably don't want to use this directly. The `Railyard` module creates and
  terminates resources processes on demand, depending on the specs that are given to it.
  Go look at the `Railyard` module first, and if you're still curious how this works,
  come back here.

  Railyard.Resource defines the GenServer which will be responsible for supervising
  a specific instance of a resource. A `Railyard.Resource` requires a
  `Railyard.Resource.Spec` to define its behavior.

  Starting a resource does not necessarily perform an action. Resources can either
  be started when none existed, or a resource can be started on top of an existing
  resource, such as running `mix railyard` to provision cloud infrastructure.
  """
  alias Railyard.Resource
  alias Railyard.Resource.Spec
  alias Railyard.Provider
  use GenServer, restart: :transient

  @doc """
  See `Railyard.Resource.Spec.eval_property/3`.
  """
  @spec eval_property(struct(), atom(), Keyword.t()) :: {atom(), any()}
  def eval_property(%_{spec: %Spec{} = spec}, prop, bindings) do
    Spec.eval_property(spec, prop, bindings)
  end

  @doc """
  See `Railyard.Resource.Spec.eval_property/3`.

  Raises an exception if property evaluation fails.
  """
  @spec eval_property!(struct(), atom(), Keyword.t()) :: any()
  def eval_property!(%_{} = rc, prop, bindings) do
    with {^prop, value} <- eval_property(rc, prop, bindings) do
      value
    else
      {:error, e} -> raise e
      e -> raise :error
    end
  end

  @doc """
  See `Railyard.Resource.Spec.eval_property/3`.

  If the property is not set on the resource, returns the default value.
  """
  @spec eval_property(struct(), atom(), Keyword.t(), any()) :: any()
  def eval_property(%_{} = rc, prop, bindings, default) do
    with {^prop, value} <- eval_property(rc, prop, bindings) do
      value
    else
      {:error, e} -> {:error, e}
      nil -> default
    end
  end

  @doc """
  Ship this resource to the running infrastructure.

  In general, this will cause resources to be provisioned if they don't exist,
  or brought into compliance with the current resource spec and parameter bindings
  if it does.

  The specific behavior of "shipping" a resource depends on the type of resource.
  See `Railyard.Provider` for provider details.
  """
  def ship(pid, bindings) when is_pid(pid) do
    GenServer.call(pid, {:ship, bindings})
  end

  def ship(%Spec{} = spec, bindings) do
    spec
    |> get_or_create_by_spec()
    |> ship(bindings)
  end

  @doc """
  Perform an action on a resource.

  The specific behavior of "shipping" a resource depends on the action
  and thetype of resource. See `Railyard.Provider` for provider details.
  """

  def act(pid, action, bindings) when is_pid(pid) do
    GenServer.call(pid, {:action, action, bindings})
  end

  def act(%Spec{} = spec, action, bindings) do
    spec
    |> get_or_create_by_spec()
    |> act(action, bindings)
  end

  @doc """
  Delete this resource from the running infrastructure.

  In general, this will deprovision, delete, remove, or otherwise destroy the
  resource. This should be considered a permanent and unrecoverable action.

  If the provider returns :ok, then the GenServer will be shut down as well.
  If not, the genserver will stay alive, and the return value (assumed to be
  an error) will be returned to the caller.
  """
  def delete(pid) when is_pid(pid) do
    case GenServer.call(pid, :delete) do
      :ok ->
        Process.exit(pid, :shutdown)
        :ok

      anything_else ->
        anything_else
    end
  end

  def delete(%Spec{} = spec) do
    spec
    |> get_or_create_by_spec()
    |> delete()
  end

  @doc """
  Given a Spec, looks up a resource's PID.
  """
  def get_or_create_by_spec(%Spec{} = resource_spec) do
    resource_id =
      resource_spec
      |> Spec.id()

    Railyard.Registry
    |> Registry.lookup(resource_id)
    |> case do
      [] ->
        with {:ok, pid} <-
               DynamicSupervisor.start_child(
                 Railyard.ResourceSupervisor,
                 %{
                   id: resource_id,
                   start: {Resource, :start_link, [resource_spec]},
                   restart: :transient
                 }
               ) do
          pid
        else
          e -> e
        end

      [{pid, nil}] ->
        pid
    end
  end

  def start_link(%Spec{} = resource_spec) do
    resource_id =
      resource_spec
      |> Spec.id()
      |> Railyard.via_tuple()

    GenServer.start_link(__MODULE__, resource_spec, name: resource_id)
  end

  @doc false
  @impl true
  def init(%Spec{type: type, block: block} = spec) do
    resource =
      Kernel.struct!(type,
        spec: spec,
        state: nil,
        pid: self()
      )

    {:ok, resource}
  end

  @doc false
  @impl true
  def handle_call(
        :read,
        _from,
        %_{state: state} = resource
      ) do
    {:reply, state, resource}
  end

  @impl true
  def handle_call(
        {:ship, bindings},
        _from,
        %_{
          spec: %Spec{type: type},
          state: state
        } = resource
      ) do
    resource
    |> Provider.ship(bindings, state)
    |> process_reply(resource)
  end

  @impl true
  def handle_call(
        {:action, action, bindings},
        _from,
        %_{
          spec: %Spec{type: type},
          state: state
        } = resource
      ) do
    resource
    |> Provider.action(action, bindings, state)
    |> process_reply(resource)
  end

  @impl true
  def handle_call(
        :delete,
        _from,
        %_{
          spec: %Spec{type: type} = spec,
          state: state
        } = resource
      ) do
    resource
    |> Provider.cleanup(state)
    |> process_reply(resource)
  end

  @impl true
  def handle_call(_anything, _from, nil) do
    {:error, "Resource can't process messages after it's been deleted.", nil}
  end

  defp process_reply({:ok, new_state}, resource) do
    {:reply, {:ok, new_state}, Kernel.struct(resource, state: new_state)}
  end

  defp process_reply(reply, resource) do
    {:reply, reply, resource}
  end
end
