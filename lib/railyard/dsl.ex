defmodule Railyard.Dsl do
  @moduledoc """
  Use `Railyard.Dsl` in a module to turn it into a **resource module**.
  """
  alias Railyard.Resource.Spec

  defmacro __using__(_opts \\ []) do
    quote do
      require Railyard.Util.BlockMap
      require Railyard.Dsl
      import Railyard.Dsl, only: [resource: 3, resource: 4]

      alias Railyard.Cfn.Fn, as: Fn

      @dsl_resources []
      @ship_before []
      @ship_after []

      @before_compile Railyard.Dsl

      :ok
    end
  end

  defmacro __before_compile__(env) do
    generate_dsl_functions(env)
  end

  defp generate_dsl_functions(_env) do
    quote do
      def ship(bindings) do
        resources()
        |> Enum.each(fn %Railyard.Resource.Spec{name: name, module: mod, type: type} ->
          is_provider =
            type
            |> Kernel.apply(:__info__, [:attributes])
            |> Keyword.get(:railyard_is_provider, false)

          if !is_provider do
            raise "#{mod} can't be shipped because resource `#{name}` of type `#{type}` is a foreign resource type."
          end
        end)

        # before_results =
        # @ship_before
        # |> Enum.map(fn mod -> {mod, apply(mod, :ship, bindings)} end)

        results =
          resources()
          |> Enum.reject(fn rc ->
            case Spec.eval_property(rc, :skip_if, bindings) do
              {:skip_if, value} -> value
              _ -> false
            end
          end)
          |> Railyard.ship(bindings)

        # after_results =
        # @ship_after
        # |> Enum.map(fn mod -> {mod, apply(mod, :ship, bindings)} end)

        %{
          # before: before_results,
          # after: after_results,
          results: results
        }
      end

      def resources() do
        @dsl_resources
        |> Enum.map(fn name -> Kernel.apply(__MODULE__, name, []) end)
      end
    end
  end

  @doc """
  Defines a **resource spec** within the module.

  Name
  """
  defmacro resource(name, type, attributes_or_block, block_if_has_attributes \\ nil) do
    {attributes, block} =
      case {attributes_or_block, block_if_has_attributes} do
        {[do: block], nil} -> {[], block}
        {attributes, [do: block]} -> {attributes, block}
      end

    quote do
      @dsl_resources [unquote(name) | @dsl_resources]

      def unquote(name)() do
        %Railyard.Resource.Spec{
          name: unquote(name),
          type: unquote(type),
          attributes: unquote(attributes),
          block: Railyard.Util.BlockMap.create_block(do: unquote(block)),
          module: __MODULE__
        }
      end
    end
  end

  defmacro ship_before(module) do
    quote do
      @ship_before [module | @ship_before]
    end
  end

  defmacro ship_after(module) do
    quote do
      @ship_after [module | @ship_after]
    end
  end
end
