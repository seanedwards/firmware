defmodule Railyard.Util.BlockMap do
  defstruct [:env, :properties]

  @moduledoc """
  Provides a DSL for creating block templates, which can be hydrated with parameters.
  """

  @type property_name :: atom()
  @type property_evaluated :: {property_name, any()}
  @type context :: %Railyard.Util.BlockMap{} | {binary(), Macro.t()}
  @type bindings :: keyword()

  @type t :: %Railyard.Util.BlockMap{
          env: Macro.Env.t(),
          properties: [{atom(), function()}]
        }

  defmacro __using__(_opts \\ []) do
    quote do
      require Railyard.Util.BlockMap
    end
  end

  @doc """
  Creates a DSL block, which can later be evaluated to produce a map.

  ## Examples

      iex> Railyard.Util.BlockMap.create_block do
      ...>   prop "value"
      ...> end
      ...> |> Railyard.Util.BlockMap.eval_block
      [prop: "value"]
  """
  @spec create_block(do: Macro.t()) :: Macro.t()
  defmacro create_block(do: block) do
    quote do
      %Railyard.Util.BlockMap{
        env: unquote(Macro.escape(__CALLER__)),
        properties: unquote(parse_block(block))
        # statement: unquote(Macro.escape(block))
      }
    end
  end

  def parse_block({:__block__, _meta, list_of_statements}) do
    list_of_statements
    |> Enum.map(&parse_line/1)
  end

  def parse_block({single_property, _meta, value} = line) when is_atom(single_property) do
    [parse_line(line)]
  end

  def parse_line({prop_name, meta, [[do: statement]]}) when is_atom(prop_name) do
    parse_line({prop_name, meta, statement})
  end

  def parse_line({prop_name, meta, [statement]}) when is_atom(prop_name) do
    parse_line({prop_name, meta, statement})
  end

  def parse_line({prop_name, _meta, statement}) when is_atom(prop_name) do
    fixed_statement =
      statement
      |> Macro.prewalk(fn
        {:params, _meta, nil} -> quote do: params
        ast -> ast
      end)

    quote do
      {
        unquote(prop_name),
        fn params ->
          unquote(fixed_statement)
        end
      }
    end
  end

  @doc """
  Evaluates a block that was previously created with `create_block`.

  ## Examples

      iex> Railyard.Util.BlockMap.create_block do
      ...>   prop "value"
      ...>   prop2 params.some_parameter
      ...>   prop3 do
      ...>     "anything"
      ...>   end
      ...> end
      ...> |> Railyard.Util.BlockMap.eval_block(some_parameter: 1234)
      [prop: "value", prop2: 1234, prop3: "anything"]
  """
  @spec eval_block(context, bindings) :: [property_evaluated]
  def eval_block(
        %Railyard.Util.BlockMap{env: env, properties: list_of_statements},
        bindings \\ []
      ) do
    list_of_statements
    |> Enum.map(fn
      {name, f} -> {name, f.(bindings |> Enum.into(%{}))}
      other -> IO.inspect(other)
    end)
  end

  @doc """
  Evaluates a single property out of a block.

  ## Examples

      iex> Railyard.Util.BlockMap.create_block do
      ...>   prop test: "value", test2: "value2"
      ...>   prop2 "value2"
      ...> end
      ...> |> Railyard.Util.BlockMap.eval_property(:prop, [])
      {:prop, [test: "value", test2: "value2"]}

      iex> Railyard.Util.BlockMap.create_block do
      ...>   prop "value"
      ...> end
      ...> |> Railyard.Util.BlockMap.eval_property(:prop, [])
      {:prop, "value"}

      iex> Railyard.Util.BlockMap.create_block do
      ...>   prop "value", "value2"
      ...> end
      ...> |> Railyard.Util.BlockMap.eval_property(:prop, [])
      {:prop, ["value", "value2"]}

      iex> Railyard.Util.BlockMap.create_block do
      ...>   prop "value", "value2"
      ...> end
      ...> |> Railyard.Util.BlockMap.eval_property(:prop2, [])
      {:error, "No such property prop2"}
  """

  @spec eval_property(context, property_name, bindings) :: property_evaluated
  def eval_property(
        %Railyard.Util.BlockMap{env: env, properties: list_of_statements},
        property,
        bindings
      ) do
    list_of_statements
    |> Keyword.fetch(property)
    |> case do
      :error -> {:error, "No such property #{property}"}
      {:ok, f} -> {property, f.(bindings |> Enum.into(%{}))}
    end
  end
end
