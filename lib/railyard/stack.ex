defmodule Railyard.Stack do
  use Railyard.Util
  use Railyard.Resource
  defstruct [
    :function,
    :supervisor
  ]

  require Railyard.Stack.Macros
  defmacro define(do: block) do
    Railyard.Stack.Macros.define(do: block)
  end


  def init(starter) do
    pid = self()
    Task.start_link(fn ->
      output = starter.function.()
      Railyard.Stack.ship(pid, output: output)
    end)
    {:ok, %__MODULE__{starter | pid: pid}}
  end

  def new(definer) do
    %__MODULE__{} = definer.()
  end

  def add_resource(nil, resource) do
    add_resource(new(), resource)
  end

  def add_resource(%__MODULE__{} = stack, resource) do
    %__MODULE__{
      resources: Map.put(stack.resources, resource.id, resource)
    }
  end
end

defmodule Railyard.Stack.Macros do
  defmacro define(do: block) do
    quote do
      %Railyard.Stack{
        function:
          fn ->
            require Railyard.Stack.Macros
            import Railyard.Stack.Macros
            _key = :output
            unquote(block)
          end
      }
    end
  end

  defmacro resource(resource_module, id, args_kwlist) do
    def_resource(resource_module, id, args_kwlist)
  end
  defp def_resource(resource_module, {k, v}, args_kwlist) do
    def_resource(resource_module, nil, [{k, v} | args_kwlist])
  end
  defp def_resource(resource_module, nil, args_kwlist) do
    def_resource(resource_module, UUID.uuid4(), args_kwlist)
  end
  defp def_resource(resource_module, id, args_kwlist) do
    args =
      args_kwlist
      |> Enum.map(fn {key, value} ->
        quote do
          {unquote(key), fn _key ->
            unquote(value)
          end}
        end
      end)
    
    quote do
      resource_module.start_link(unquote(args))
    end
  end

  @spec ref(handle :: term()) :: map()
  defmacro ref(pid) do
    receives_updates = self()
    to_property = _key
    # Task.await...
  end

end