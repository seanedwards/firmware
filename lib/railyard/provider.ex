defprotocol Railyard.Provider do
  @moduledoc """
  The protocol that a Railyard provider must implement in order to create resources
  of that type. See `Railyard.Fs.File` for a simple example.
  """
  @fallback_to_any true

  @type state :: any()
  @type action :: any()
  @type error :: {:error, String.t()}
  @type ok_or_error :: {:ok, state} | error
  @type resource :: any()
  @type bindings :: list()

  @doc """
  Push the spec and bindings to the running infrastructure. This is a combined "upsert" type of action.
  If the resource does not exist, it should be created. If it does exist, it should be updated according to the params.
  """
  @spec ship(resource, bindings, state) :: ok_or_error
  def ship(resource, bindings, state)

  @doc """
  Perform an action on the resource. This callback is optional, and may be omitted.
  """
  @spec action(resource, action, bindings, state) :: ok_or_error
  def action(resource, action, bindings, state)

  @doc """
  Destroy, delete, terminate, etc. any associated physical resources.
  The resource is being permanently removed from the infrastructure.
  """
  @spec cleanup(resource, state) :: :ok | error
  def cleanup(resource, state)
end

defimpl Railyard.Provider, for: Any do
  def ship(%_{spec: %Railyard.Resource.Spec{name: name, type: type}}, bindings, _state) do
    {:error, "Nothing to ship for #{name} (#{type})"}
  end

  def action(%_{spec: %Railyard.Resource.Spec{name: name, type: type}}, action, _bindings, _state) do
    {:error, "Action #{action} has not been defined on #{name} (#{type})"}
  end

  def cleanup(resource, state), do: :ok
end

# From https://github.com/jeremyjh/dialyxir/issues/221#issuecomment-460398948
#
# The following is a hack to suppress dialyzer errors/warnings:
# - error messages appear for unimplemented types when running 'mix dialyzer'
# - for each type, error message looks like...
#   _____________________________________________________________________________
#   :0:unknown_function
#   Function MyProtocolModule.SomeType.__impl__/1 does not exist.
# - this hack should (probably) be defined after all implementations
:ok =
  (fn protocol_module ->
     [
       Atom,
       BitString,
       Float,
       Function,
       Integer,
       List,
       Map,
       PID,
       Port,
       Reference,
       Tuple
     ]
     |> Enum.each(fn type ->
       try do
         _ = Module.concat(protocol_module, type).module_info
       rescue
         _ ->
           defimpl protocol_module, for: type do
             defdelegate ship(rc, bindings, state), to: Railyard.Provider.Any
             defdelegate action(rc, action, bindings, state), to: Railyard.Provider.Any
             defdelegate cleanup(rc, state), to: Railyard.Provider.Any
           end
       end
     end)
   end).(Railyard.Provider)
