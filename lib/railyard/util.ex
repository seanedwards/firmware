defmodule Railyard.Util do
  @moduledoc false

  defmacro __using__(_) do
    quote do
      alias Railyard.Resource
      alias Railyard.Resource.Spec

      require Railyard.Util
      import Railyard.Util
    end
  end
end
