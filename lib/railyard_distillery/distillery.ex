defmodule Railyard.Distillery do
  @moduledoc """
  The `Railyard.Distillery` module provides Railyard integration with Distillery, allowing
  the deployment lifecycle of an elixir app to be managed by Railyard.
  """
  use Railyard.Resource

  @mix_project_config Mix.Project.config()

  def tar_gz_path() do
    project_name = @mix_project_config[:app]
    project_version = @mix_project_config[:version]

    Mix.Project.build_path() <>
      "rel/#{project_name}/releases/#{project_version}/#{project_name}.tar.gz"
  end

  @spec download_s3(String.t(), String.t(), String.t()) :: {:ok, term} | {:error, term}
  def download_s3(local_path, bucket, s3_path) do
    bucket
    |> ExAws.S3.download_file(s3_path, local_path)
    |> ExAws.request()
  end

  defimpl Railyard.Provider do
    alias Railyard.Resource
    alias Railyard.Distillery

    def ship(rc, bindings, _state) do
      bindings =
        bindings
        |> Keyword.put(:distillery, rc)

      Railyard.Distillery.ServerConfig.ship(bindings)
      {:ok, nil}
    end

    def cleanup(rc_spec, %{path: path}) do
      :ok
    end

    def action(rc, action, bindings, state) do
      # Send the action to...
      # Railyard.Distillery.ServerConfig.railyard()
      {:ok, state}
    end
  end
end
