defmodule Railyard.Distillery.ServerConfig do
  @moduledoc false

  use Railyard.Dsl
  alias Railyard.Resource

  # resource :tar_gz, Railyard.Fs.File do
  # path do
  # version = Resource.eval_property!(props.distillery, bindings, :version)
  # "/opt/app-#{version}.tar.gz"
  # end

  # download(Resource.eval_property!(props.distillery, bindings, :download))
  # end

  # resource :untar, Railyard.Cmd.Shell, depends_on: :tar_gz do
  # script do
  # version = Resource.eval_property!(props.distillery, bindings, :version)

  # """
  ##! /usr/bin/env bash -e
  # tar -xzf --overwrite /opt/app-#{version}.tar.gz
  # """
  # end
  # end

  # resource :railyard, Railyard.Cmd.Elixir, depends_on: :untar do
  # download do
  # parsed_uri =
  # props.distillery
  # |> Resource.eval_property(bindings, :url)
  # |> URI.parse()

  # case parsed_uri do
  # %URI{scheme: "s3", host: bucket, path: s3_path} ->
  # Railyard.Distillery.download_s3(bucket, s3_path)

  # _ ->
  # nil
  # end
  # end

  # unpack do
  # end

  # restart do
  # Resource.eval_property(distillery, bindings, :before_restart)
  # :init.restart()
  # end

  # reboot do
  # Resource.eval_property(distillery, bindings, :before_reboot)
  # :init.reboot()
  # end
  # end
end
