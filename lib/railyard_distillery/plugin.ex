defmodule Railyard.Distillery.Plugin do
  @moduledoc """
  Provides a distillery plugin for adding config management lifecycle commands to the
  distillery build, and optionally for uploading packages to a file host like S3.
  """
  use Mix.Releases.Plugin

  def after_assembly(
        %Release{
          profile:
            %Mix.Releases.Profile{
              overlays: overlays
            } = profile
        } = release,
        _opts
      ) do
    # Add the custom `railyard` command here

    railyard_overlays = []
    # or nil
    %Release{
      release
      | profile: %Mix.Releases.Profile{profile | overlays: railyard_overlays ++ overlays}
    }
  end

  def after_package(%Release{} = release, _opts) do
    # Upload the package to S3 here
    info("This is executed just after packaging the release")
    # or nil
    release
  end

  def before_package(release, _b) do
    release
  end

  def before_assembly(release, _b) do
    release
  end
end
