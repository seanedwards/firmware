require Railyard.Aws.Client

Railyard.Aws.Client.load(
  cloudformation: "2010-05-15",
  iam: "2010-05-08",
  # s3: "2006-03-01",
  # ssm: "2014-11-06",
  sts: "2011-06-15"
)

defmodule Railyard.Cfn do
  @moduledoc """
  Provides a rich, metaprogramming style elixir interface for writing and launching Cloudformation templates.
  """
  IO.puts("""
  Starting compile of Railyard.Cfn. Sorry, this will take a while.
  """)

  # use Railyard.Cfn.Generator, "priv/cfn/us-west-2.json"
  # use Railyard.Util.BlockMap
end
