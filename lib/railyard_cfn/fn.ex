defmodule Railyard.Cfn.Fn do
  def ref(resource) do
    %{"Fn::Ref" => resource}
  end

  @spec fixup(Macro.t()) :: Macro.t()
  def fixup(arbitrary_code) do
    Macro.prewalk(arbitrary_code, &fixup_fn/1)
  end

  defp fixup_fn(
         {{:., _meta,
           [
             {:__aliases__, aliases, [:Fn]},
             :get_att
           ]}, meta, [rc, att]}
       ) do
    {:%{}, meta,
     [
       {"Fn::GetAtt", [rc |> to_string |> Macro.camelize(), att]}
     ]}
  end

  defp fixup_fn(
         {{:., _meta,
           [
             {:__aliases__, _aliases, [:Fn]},
             :ref
           ]}, meta, [arg]}
       ) do
    {:%{}, meta,
     [
       {"Fn::Ref", arg |> to_string |> Macro.camelize()}
     ]}
  end

  # Fn.*
  defp fixup_fn(
         {{:., _meta,
           [
             {:__aliases__, _aliases, [:Fn]},
             function
           ]}, meta, [arg]}
       ) do
    fn_name = fixup_fn_name(function)

    {:%{}, meta,
     [
       {fn_name, arg}
     ]}
  end

  defp fixup_fn(
         {{:., _meta,
           [
             {:__aliases__, _aliases, [:Fn]},
             function
           ]}, meta, args}
       ) do
    fn_name = fixup_fn_name(function)

    {:%{}, meta,
     [
       {fn_name, args}
     ]}
  end

  defp fixup_fn(node) do
    node
  end

  defp fixup_fn_name("get_azs"), do: "Fn::GetAZs"

  defp fixup_fn_name(name) do
    "Fn::" <> Macro.camelize(to_string(name))
  end
end
