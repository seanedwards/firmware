defmodule Railyard.Cfn.Stack do
  @moduledoc """
  """
  use Railyard.Resource

  def get_module_specs(stack_module, bindings) do
    stack_module.resources()
    |> Enum.map(fn {name, spec} ->
      {name, build_properties(spec, bindings)}
    end)
    |> Enum.into(%{})
  end

  def get_module_template(stack_module, bindings) do
    resources =
      stack_module
      |> get_module_resources(bindings)

    %{
      "AWSTemplateFormatVersion" => "2010-09-09",
      "Resources" => resources
    }
  end

  def get_module_resources(stack_module, bindings) do
    stack_module.resources()
    |> Enum.map(fn %_{name: name} = spec ->
      {name |> to_string |> Macro.camelize(), build_resource(spec, bindings)}
    end)
    |> Enum.into(%{})
  end

  defp build_properties(%Railyard.ResourceSpec{} = spec, bindings) do
    spec.type.from_spec(spec, bindings)
  end

  defp build_resource(%Railyard.ResourceSpec{} = spec, bindings) do
    cfn_type = spec.type.cfn_type_string()
    cfn_struct = build_properties(spec, bindings)
    cfn_resource = spec.type.to_cfn(cfn_struct)
    attributes = build_attributes(spec.attributes)

    [
      {"Type", cfn_type},
      {"Properties", cfn_struct}
      | attributes
    ]
    |> Enum.into(%{})
  end

  defp build_attributes(attributes) do
    attributes
    |> Enum.map(fn {k, v} ->
      camelized_key =
        k
        |> to_string
        |> Macro.camelize()

      {camelized_key, v}
    end)
  end

  def opts_for_operation(rc, bindings, op_keys) do
    op_keys
    |> Enum.map(fn key ->
      case Resource.eval_property(rc, key, bindings) do
        {^key, value} -> {key, value}
        _ -> nil
      end
    end)
    |> Enum.filter(&(&1 != nil))
  end

  defimpl Railyard.Provider do
    alias Railyard.Cfn.Stack
    alias Railyard.Resource

    use ExAws.Utils,
      format_type: :xml,
      non_standard_keys: %{
        stack_name: "StackName",
        role_arn: "RoleARN",
        template_url: "TemplateURL",
        next_token: "NextToken",
        parameters: "Parameters.member",
        resource_types: "ResourceTypes.member",
        retain_resources: "RetainResources.member",
        notification_arns: "NotificationARN.member",
        skip_resources: "ResourcesToSkip.member"
      }

    @version "2010-05-15"

    def ship(rc, bindings, state) do
      {:stack_name, stack_name} = Resource.eval_property(rc, :stack_name, bindings)
      {:stack_module, stack_module} = Resource.eval_property(rc, :stack_module, bindings)

      cfn_template =
        stack_module
        |> Stack.get_module_template(bindings)
        |> Jason.encode!(pretty: true)
        |> IO.inspect()

      if state[:created] do
        stack_params =
          Stack.opts_for_operation(rc, bindings, [])
          |> Enum.map(&upcase/1)

        %ExAws.Operation.Query{
          path: "/",
          params: [{"StackName", stack_name} | stack_params],
          service: :cloudformation,
          action: "UpdateStack",
          parser: &ExAws.Cloudformation.Parsers.parse/3
        }
      else
        ExAws.Cloudformation.create_stack(
          stack_name,
          Stack.opts_for_operation(rc, bindings, [
            :capabilities,
            :disable_rollback,
            :notification_arns,
            :on_failure,
            :parameters,
            :resource_types,
            :role_arn,
            :stack_policy_body,
            :stack_policy_url,
            :tags,
            :template_url,
            :timeout_in_minutes
          ]) ++
            [
              template_body: cfn_template
            ]
        )
      end
      |> ExAws.request()
      |> IO.inspect()

      new_state = %{state | created: true, stack_name: stack_name}
      {:ok, new_state}
    end

    def cleanup(rc, state) do
      ExAws.Cloudformation.delete_stack(
        state.stack_name,
        []
      )
      |> ExAws.request()
    end

    def action(rc, :to_cfn, bindings, state) do
    end
  end
end
