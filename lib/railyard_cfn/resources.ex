defmodule Railyard.Cfn.Generator do
  @moduledoc """
  The Elixir metaprogramming code for generating native Elixir modules and functions, which represent the
  various Cloudformation resources supported by AWS.
  """
  defmacro __using__(json) do
    json
    |> File.read!()
    |> Jason.decode!()
    |> generate_resources
  end

  def generate_resources(%{
        "ResourceSpecificationVersion" => spec_version,
        "PropertyTypes" => property_types,
        "ResourceTypes" => resources
      }) do
    types_by_resource =
      property_types
      |> Map.to_list()
      |> Enum.group_by(fn
        {"Tag", _type_spec} ->
          nil

        {type_name, _type_spec} ->
          type_name
          |> String.split(".")
          |> hd
      end)

    generated_resources =
      for {type, resource_spec} <- resources do
        resource_types =
          types_by_resource
          |> Map.get(type)

        generate_resource(type, resource_spec, resource_types)
      end

    quote do
      (unquote_splicing(generated_resources))
    end
  end

  def generate_resource(
        module_name,
        %{
          "Properties" => property_spec,
          "Documentation" => module_doc
        } = resource_spec,
        resource_types
      ) do
    module_atom = module_name |> cfn_to_module
    struct_def = generate_properties(module_name, property_spec)
    module_types = generate_types(resource_types)

    quote do
      defmodule unquote(module_atom) do
        @behaviour Railyard.Cfn.Resource

        @moduledoc """
        ## [#{unquote(module_name)}](#{unquote(module_doc)})
        """
        @cfn_name unquote(module_name)
        unquote(struct_def)

        unquote_splicing(module_types)

        @doc """
        Returns `#{@cfn_name}`
        """
        @impl true
        def cfn_type_string() do
          @cfn_name
        end

        defimpl Jason.Encoder, for: unquote(module_atom) do
          @moduledoc false

          @doc false
          def encode(struct, opts) do
            Jason.Encode.map(unquote(module_atom).to_cfn(struct), opts)
          end
        end
      end
    end
  end

  def generate_types(nil), do: []

  def generate_types(type_specs) do
    type_specs
    |> Enum.map(fn {type_name, type_spec} ->
      [resource_name, cfn_typespec_name | _rest] = type_name |> String.split(".")

      typespec_name = cfn_to_atom("t" <> cfn_typespec_name)
      typespec_value = type_of(resource_name, type_spec)

      quote do
        @type unquote(typespec_name)() :: unquote(typespec_value)
      end
    end)
  end

  def generate_properties(module_name, %{} = properties) do
    properties_defs =
      properties
      |> Enum.map(fn {string_property_name,
                      %{
                        "Required" => property_required,
                        "Documentation" => property_doc,
                        "UpdateType" => property_update_type
                      } = property_spec} ->
        property_name =
          case string_property_name do
            # Some property names conflict with elixir key words like try/catch/after
            "After" -> "After_"
            name -> name
          end
          |> cfn_to_atom

        property_type = type_of(module_name, property_spec)

        %{
          struct: property_name,
          enforce_keys:
            if property_required do
              property_name
            end,
          parser:
            quote do
              {
                unquote(property_name),
                block
                |> Railyard.Util.BlockMap.eval_property(unquote(property_name), bindings)
                |> case do
                  {unquote(property_name), val} -> unquote(property_name)(val)
                  _ -> nil
                end
              }
            end,
          serializer:
            quote do
              {unquote(string_property_name), Map.fetch!(resource, unquote(property_name))}
            end,
          attrs_t:
            quote do
              {unquote(property_name), unquote(property_type)}
            end,
          methods:
            quote do
              @spec unquote(property_name)(unquote(property_type)) :: unquote(property_type)
              defp unquote(property_name)(value) do
                value
              end
            end,
          moduledoc:
            quote do
              """
              ### [#{unquote(string_property_name)}](#{unquote(property_doc)})
              * Type: #{unquote(cfn_to_doc(module_name, property_spec))}
              * Required: #{unquote(property_required)}
              * Update Type: #{unquote(property_update_type)}
              """
            end
        }
      end)

    quote do
      @moduledoc unquote(properties_defs |> map_get(:moduledoc)) |> Enum.join("\n\n")
      @type t :: %__MODULE__{unquote_splicing(properties_defs |> map_get(:attrs_t))}
      @enforce_keys unquote(
                      properties_defs
                      |> map_get(:enforce_keys)
                      |> Enum.filter(&(&1 != nil))
                    )

      defstruct unquote(properties_defs |> map_get(:struct))

      unquote_splicing(properties_defs |> map_get(:methods))

      @impl true
      def to_cfn(%__MODULE__{} = resource) do
        unquote(properties_defs |> map_get(:serializer))
        |> Enum.filter(fn {_prop_name, prop_value} -> prop_value != nil end)
        |> Enum.into(%{})
      end

      @doc """
      Converts a conforming `Railyard.ResourceSpec` into an`#{unquote(cfn_to_doc(module_name))}`
      """
      @spec from_spec(Railyard.ResourceSpec.t(), keyword()) :: __MODULE__.t()
      def from_spec(%Railyard.ResourceSpec{block: block, type: type}, bindings) do
        %__MODULE__{
          unquote_splicing(
            properties_defs
            |> map_get(:parser)
          )
        }
      end
    end
  end

  defp map_get(list, prop) do
    list
    |> Enum.map(fn i -> Map.get(i, prop) end)
  end

  def cfn_to_atom(name) do
    name |> Macro.underscore() |> String.to_atom()
  end

  def cfn_to_module(name) do
    replaced = name |> String.replace("::", ".")
    atomized = "Elixir." <> replaced
    atomized |> String.to_atom()
  end

  def cfn_to_doc("Tag") do
    "Railyard.Cfn.Tag"
  end

  def cfn_to_doc(name) do
    name
    |> String.replace("::", ".")
  end

  def cfn_to_doc(type, name) when is_binary(name) do
    Macro.underscore("t" <> name)
  end

  def cfn_to_doc(module_name, %{"Type" => type, "PrimitiveItemType" => item_type}) do
    "#{type} of #{item_type}"
  end

  def cfn_to_doc(module_name, %{"Type" => type, "ItemType" => item_type}) do
    "`#{type}` of `#{cfn_to_doc(module_name, item_type)}`"
  end

  def cfn_to_doc(module_name, %{"Type" => type}) do
    "`#{cfn_to_doc(module_name, type)}`"
  end

  def cfn_to_doc(module_name, %{"PrimitiveType" => type}) do
    "`#{type}`"
  end

  defp cfn_resource_type(name) do
    name
    |> String.split(".")
    |> hd
  end

  defp type_of(source, %{"Properties" => cfn_fields}) do
    ex_fields =
      cfn_fields
      |> Map.to_list()
      |> Enum.map(fn {field_name, field_spec} ->
        optional_or_required =
          case Map.get(field_spec, "Required") do
            true -> :required
            _ -> :optional
          end

        quote do
          {
            unquote(optional_or_required)(unquote(field_name |> cfn_to_atom)),
            unquote(type_of(source, field_spec))
          }
        end
      end)

    quote do
      %{
        unquote_splicing(ex_fields)
      }
    end
  end

  defp type_of(source, %{"Type" => "List", "ItemType" => item_type}) do
    quote do: list(unquote(type_of(source, item_type)))
  end

  defp type_of(source, %{"Type" => "List", "PrimitiveItemType" => item_type}) do
    quote do: list(unquote(type_of(source, item_type)))
  end

  defp type_of(source, %{"Type" => complex_type}) do
    type_of(source, complex_type)
  end

  defp type_of(source, %{"PrimitiveType" => basic_type}) do
    type_of(source, basic_type)
  end

  defp type_of(_source, "Json"), do: quote(do: term())
  defp type_of(_source, "Map"), do: quote(do: map())
  defp type_of(_source, "Timestamp"), do: quote(do: String.t())
  defp type_of(_source, "String"), do: quote(do: String.t())
  defp type_of(_source, "Integer"), do: quote(do: integer())
  defp type_of(_source, "Long"), do: quote(do: integer())
  defp type_of(_source, "Double"), do: quote(do: float())
  defp type_of(_source, "Boolean"), do: quote(do: boolean())
  defp type_of(_source, "Tag"), do: quote(do: Railyard.Cfn.Tag.t())

  defp type_of(source, type_name) when is_binary(type_name) do
    atom_module = source |> cfn_resource_type |> cfn_to_module
    atom_type = cfn_to_atom("t" <> type_name)
    quote do: unquote(atom_type)()
  end

  defp type_of(_source, anything_else) do
    quote do: term()
  end
end

defmodule Railyard.Cfn.Resource do
  @callback cfn_type_string() :: String.t()
  @callback to_cfn(any()) :: map()
end
